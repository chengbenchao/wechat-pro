const Http = require("./Http");
class MovieHttp  extends Http{
    /* top250 || inTheaters || comingSoon */
    static getMovie(m){
        return MovieHttp.request({
            url:`/api/movie/${m}`
        })
    }

    static setCollect({id,m,collected}){
        return MovieHttp.request({
            url:`/api/collectMovie/${m}`,
            data:{
                id,
                collected
            },
            method:"post"
        })
    }

    static addHistory({id,m}){
        return MovieHttp.request({
            url:`/api/history?id=${id}&m=${m}`
        })
    }
    
    static getSearch(keyword){
        return MovieHttp.request({
            url:`/api/search?keyword=${keyword}`
        })
    }

    static getDetail(id){
        return MovieHttp.request({
            url:`/api/movieDetail?id=${id}`
        })
    }
}
module.exports = MovieHttp;