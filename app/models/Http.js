const baseUrl = "http://47.108.197.28:4000"
class Http {
    static request({
        url,
        data,
        method = "get"
    }) {
        return new Promise((resolve, reject) => {
            wx.request({
                url: baseUrl + url,
                data,
                header: {
                    'content-type': 'application/json'
                },
                method,
                dataType: 'json',
                responseType: 'text',
                success: (res) => {
                    /* 能够连接上网络 */
                    this.handleStatusCode(res,resolve)
                },
                fail: (res) => {
                    /* 不能连接上网络 */
                    this.handleStatusCode(res,reject)
                }
            });
        })
    }
    static handleStatusCode(res,params) {
        if (res.statusCode) {
            if (res.statusCode == 200 && res.data.code == 200) {
                params(res);
            } else {
                wx.showToast({
                    title: '网络异常',
                    icon: 'none'
                });
                throw new Error("传参异常");
            }
        } else {
            // 连接不上网络
            params(err);
            wx.showToast({
                title: '无法连接网络,请开启网络连接',
                icon: 'none'
            });
            throw new Error("网络异常");
        }
    }
}
module.exports = Http;