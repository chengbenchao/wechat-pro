const ArticleHttp = require("../../models/ArticleHttp")
Page({

  /**
   * 页面的初始数据
   */
  data: {
      item:"",
      title:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    var {id} = options;
    var result = await ArticleHttp.getDetail(id);
    var {res,msg} = result.data;
    wx.setNavigationBarTitle({
      title: msg
    })
    this.setData({
      item:res[0],
      title:msg
    })
  },
  /* 监听收藏状态 */
  async onCollect(){
    var item = this.data.item;
    var {collected,_id} = item
    if(collected){
      item.collected = false
      await ArticleHttp.setCollect({id:_id,collected:false});
      

    }else{
      item.collected = true;
      await ArticleHttp.setCollect({id:_id,collected:true});
    }
    this.setData({
      item
    })
  }
})