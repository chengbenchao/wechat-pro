const ArticleHttp = require("../../models/ArticleHttp");
Page({

  /**
   * 页面的初始数据
   */
  data: {
      articles:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    var data = await ArticleHttp.getlist();
    this.setData({
      articles:data.data.res
    })
  }
})