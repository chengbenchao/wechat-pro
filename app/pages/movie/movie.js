// pages/movie/movie.js
const MovieHttp = require("../../models/MovieHttp")
Page({

  /**
   * 页面的初始数据
   */
  data: {
      movie:""
  },
  onLoad: async function (options) {
    /* top250 || inTheaters || comingSoon */
      this.getMovieHttp();
  },
  async getMovieHttp(){
    var movie = {}
    var https = [{"top250":"top250"},{"inTheaters":"正在热映"},{"comingSoon":"即将上映"}];
    for(let item of https){
        var key = Object.keys(item)[0];
        var result  = await MovieHttp.getMovie(key);
        movie[key] = {
          res:result.data.res.slice(0,3),
          title:item[key]
        }
    }
    this.setData({
      movie
    })
  },
  handleMore(event){
    var {mkey} = event.currentTarget.dataset;
    wx.navigateTo({
      url: `/pages/movieDetail/index?m=${mkey}`
    })
  }
})