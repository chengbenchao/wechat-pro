// pages/movieDetail/index.js
const MovieHttp = require("../../models/MovieHttp");
Page({

  /**
   * 页面的初始数据
   */
  data: {
      movies:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    var {m} = options;
    var result = await MovieHttp.getMovie(m);
    console.log(result.data.res);
    this.setData({
      movies:result.data.res
    })
  }
})