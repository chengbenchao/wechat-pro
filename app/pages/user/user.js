// pages/user/user.js
const UserHttp = require("../../models/UserHttp");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAuth: false,
    logo: "",
    username: "",
    movies: []
  },
  onLoad:async function (options) {
    // 查看是否授权
    var self = this;
    this.getDefaultHttp();
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: (res) => {
              var userInfo = res.userInfo
              var {
                avatarUrl,
                nickName
              } = userInfo;
              self.setData({
                isAuth: true,
                logo: avatarUrl,
                username: nickName
              })
              
            }
          })
        }
      }
    })
  },
  bindGetUserInfo(e) {
    var userInfo = e.detail.userInfo;
    /* 点击确定授权才会获取用户信息 */
    if (userInfo) {
      var {
        avatarUrl,
        nickName
      } = userInfo;
      this.setData({
        isAuth: true,
        logo: avatarUrl,
        username: nickName
      })
    }
  },
  async onChange(event) {
    var name = event.detail.name
    var result;
    if (name == "getCollect") {
      var res = await UserHttp.getCollectMovie();
      result = res.data.res;
    } else {
      var res = await UserHttp.getHistoryMovie();
      result = res.data.res;
    }
    this.setData({
      movies: result
    })
  },
  async getDefaultHttp() {
    var res = await UserHttp.getCollectMovie();
    var result = res.data.res;
    this.setData({
      movies: result
    })
  }
})