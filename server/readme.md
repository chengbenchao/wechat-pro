#### 1 修改某个字段  $rename

```
db.article.updateMany({},{$rename:{collectioned:"collectNum"}})
```

#### 2  根据_id 降序

```
db.article.find().sort({_id:-1})
```

#### 3 获取记录的上一条或下一条数据

```js
db.article.find({_id:{$gt:ObjectId("xxx")}}).limit(1) //下一条
db.article.find({_id:{$lt:ObjectId("xxx")}}).limit(1) //上一条
```

