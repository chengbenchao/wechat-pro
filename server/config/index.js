const requireDir = require("require-directory");
const Router = require("koa-router");
const static = require("koa-static");
const body = require("koa-body");
const cors = require("koa2-cors");
function initManage(app) {
    /* 1.解析post请求,及图片上传 */
    app.use(body({
        multipart: true,
        formidable: {
            maxFileSize: 200 * 1024 * 1024,
            keepExtensions: true
        }
    }))
    /* 2.跨域 */
    app.use(cors());
    /* 3.静态资源 */
    app.use(static(`${process.cwd()}/images`))
    /* 4.路由的自动导入 */
    requireDir(module, `${process.cwd()}/routers`, {
        visit: loadRouter
    })
    function loadRouter(obj) {
        if (obj instanceof Router) {
            app.use(obj.routes());
        }
    }

}
module.exports = initManage;