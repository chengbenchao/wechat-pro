const router = require("koa-router")();
const ArticleModel = require("../../models/article");
router.post("/api/doCollect",async ctx=>{
    var {id,collected} = ctx.request.body;
    try{
        await ArticleModel.updateOne({_id:id},{$set:{collected}});
        ctx.body = {
            code:200,
            msg:"修改成功",
            request:"POST /api/doCollect"
        }
    }catch(err){
       ctx.body = {
           code:400,
           msg:"请求参数不合法",
           request:"POST /api/doCollect"
       }
    }
})
module.exports = router;