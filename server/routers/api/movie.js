const router = require("koa-router")();
const MovieModel = require("../../models/movie");
router.get("/api/movie/:m",async ctx=>{
    var {m} = ctx.request.params;
    var data = await MovieModel(m).find();
    if(data.length){
        ctx.body = {
            code:200,
            res:data,
            request:`请求成功 ${m}`
        }
    }else{
        ctx.body = {
            code:400,
            request:"输入的参数不合法"
        }
    }
})
module.exports = router;
