const router = require("koa-router")();
const ArticleModel = require("../../models/article");
router.get("/api/ArticleDetail",async ctx=>{
    var {id} = ctx.query;
    var res = await ArticleModel.find({_id:id});
    ctx.body = {
        code:200,
        res,
        msg:"文章详情"
    }
})
module.exports = router;
