const router = require("koa-router")();
const MovieModel = require("../../models/movie");
const UserModel = require("../../models/user");
router.post("/api/collectMovie/:m",async ctx=>{
    var {m} = ctx.request.params;  
    var {id,collected} = ctx.request.body;
    try{
        var data = await MovieModel(m).updateOne({_id:id},{$set:{collected}});
        if(data.nModified==0){
            throw new Error(`${m}这个传值不合法`)
        }else{
            /* 添加收藏数据到user */
            var item = await MovieModel(m).find({_id:id});
            if(collected){
                /* true,$push */
                await UserModel.updateOne({},{$push:{collects:item[0]}});
                ctx.body = {
                    code:200,
                    msg:"收藏成功"
                }
            }else{
                /* false,$pull */
                await UserModel.updateOne({},{$pull:{collects:{_id:id}}});
                ctx.body = {
                    code:200,
                    msg:"取消收藏发"
                }
            }
           
           
        }
    }catch(err){
        /* id,collected  走catch */
        ctx.body = {
            code:400,
            msg:"输入的参数不合法"
        }
    }

})
module.exports = router;