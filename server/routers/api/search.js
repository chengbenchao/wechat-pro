const router = require("koa-router")();
const MovieModel = require("../../models/movie");
router.get("/api/search",async ctx=>{
    var {keyword} = ctx.query;
    var reg = new RegExp(keyword);
    var tables = ["top250","inTheaters","comingSoon"];
    var searchMovies = []
    for(var item of tables){
        // var res = await MovieModel(item).find({title:{$regex:keyword}});
        var res = await MovieModel(item).find({title:reg});
        searchMovies.push(...res);
    }
    ctx.body = {
        code:200,
        res:searchMovies,
        total:searchMovies.length,
        msg:"电影搜索"
    }
})
module.exports = router;
