const {mongodb,Schema} = require("./baseDB/db-movies");
const MovieSchema = require("./Schema/MovieSchema")
const schema = new Schema(MovieSchema);
function MovieModel (table){
    return mongodb.model(table,schema,table);
}
module.exports = MovieModel;