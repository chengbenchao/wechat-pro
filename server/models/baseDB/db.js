const mongoose = require("mongoose");
/* 1.连接本地数据库 */
var mongodb = mongoose.createConnection('mongodb://test:123@47.108.197.28:27017/wechat?authSource=admin', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
var Schema = mongoose.Schema;
module.exports = {
    mongodb,
    Schema
};