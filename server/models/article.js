const {mongodb,Schema} = require("./baseDB/db")
const ArticleSchema =new Schema({
    date: String,
    title: String,
    imgSrc: String,
    avatar: String,
    content: String,
    reading: String,
    collectNum: Number,
    headImgSrc: String,
    author: String,
    dateTime: String,
    detail: String,
    postId: Number,
    music: {
        url: String,
        title: String,
        coverImg: String
    },
    collected: Boolean
})
const ArticleModel = mongodb.model("Article",ArticleSchema,"article");
module.exports = ArticleModel;