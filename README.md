# wechat-pro

## 01搭建后台服务

##### 1-1 安装依赖

```
yarn  add koa koa-router koa2-cors koa-body koa-static
yarn  add mongoose
yarn  add require-directory
```

#### 1-2 分拆项目

![](assets/init.png)

```js
//index.js
const Koa = require("koa");
const app =  new Koa();
const initManage = require("./config");
initManage(app);
app.listen(4000)
```

```js
// config/index.js
const requireDir = require("require-directory");
const Router = require("koa-router");
function initManage(app){
    requireDir(module,`${process.cwd()}/routers`,{visit:loadRouter})
    function loadRouter(obj){
        if(obj instanceof Router){
            app.use(obj.routes());
        }
    }
}
module.exports = initManage;
```

```js
// routers/api/article.js
const router = require("koa-router")();
router.get("/article",async ctx=>{
    ctx.body = "article"
})
module.exports = router;
```

## 02  连接数据库,获取数据

```
wechat-article
```



```
/GET  /api/article
```

```js
// models/db.js
const mongoose = require("mongoose");
/* 1.连接本地数据库 */
mongoose.connect('mongodb://127.0.0.1:27017/wechat', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
module.exports = mongoose;
// models/article.js
const mongoose = require("./db");
const ArticleSchema = mongoose.Schema({
    date: String,
    title: String,
    imgSrc: String,
    avatar: String,
    content: String,
    reading: String,
    collectioned: String,
    headImgSrc: String,
    author: String,
    dateTime: String,
    detail: String,
    postId: Number,
    music: {
        url: String,
        title: String,
        coverImg: String
    },
    collected: Boolean
})
const ArticleModel = mongoose.model("Article",ArticleSchema,"article");
module.exports = ArticleModel;
```

```js
/api/article
const router = require("koa-router")();
const ArticleModel = require("../../models/article");
router.get("/api/article",async ctx=>{
    var data = await ArticleModel.find({});
    ctx.body = {
        code:200,
        res:data,
        msg:"文章相关的数据",
        request:"GET /article"
    }
})
module.exports = router;
```

## 03 实现收藏状态的切换

```js
const router = require("koa-router")();
const ArticleModel = require("../../models/article");
router.post("/api/doCollect",async ctx=>{
    var {id,collected} = ctx.request.body;
    try{
        await ArticleModel.updateOne({_id:id},{$set:{collected}});
        ctx.body = {
            code:200,
            msg:"修改成功",
            request:"POST /api/doCollect"
        }
    }catch(err){
       ctx.body = {
           code:400,
           msg:"请求参数不合法",
           request:"POST /api/doCollect"
       }
    }
})
module.exports = router;
//Tips:使用try-catch,try处理成功-catch处理失败的情况
```

## 04  连接多个数据库

```js
//db.js
const mongoose = require("mongoose");
/* 1.连接本地数据库 */
var mongodb = mongoose.createConnection('mongodb://127.0.0.1:27017/wechat', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
var Schema = mongoose.Schema;
module.exports = {
    mongodb,
    Schema
};
```

```js
//article.js
const {mongodb,Schema} = require("./baseDB/db")
const ArticleSchema =new Schema({...})
const ArticleModel = mongodb.model("Article",ArticleSchema,"article");
module.exports = ArticleModel;
```

## 05 操作movies库下三张一样的表

```
top250,inTheaters,comingSoon这三张表的字段完全一样
```

```js
//1.封装Model
const {mongodb,Schema} = require("./baseDB/db-movies");
const MovieSchema = require("./Schema/MovieSchema")
const schema = new Schema(MovieSchema);
function MovieModel (table){
    return mongodb.model(table,schema,table);
}
module.exports = MovieModel;
```

```js
//2.处理前端路由  
//GET  /api/movie/:m
m = top250 || inTheaters || comingSoon
//使用动态路由去处理前端路由
router.get("/api/movie/:m",async ctx=>{
    var {m} = ctx.request.params;
    var data = await MovieModel(m).find();
    if(data.length){
        ctx.body = {
            code:200,
            res:data,
            request:`请求成功 ${m}`
        }
    }else{
        ctx.body = {
            code:400,
            request:"输入的参数不合法"
        }
    }
})
```

## 06 实现对user表中collects的收藏添加

```
1.实现电影收藏状态的切换
2.实现对user表collects的添加
```

```js
// UserModel--user.js
const {mongodb,Schema} = require("./baseDB/db-movies");
const schema = new Schema({
     name:String,
     collects:Array
});
var UserModel = mongodb.model("user",schema,"user");
module.exports = UserModel;
```

```js
//  colletMovie.js
router.post("/api/collectMovie/:m",async ctx=>{
    var {m} = ctx.request.params;  
    var {id,collected} = ctx.request.body;
    try{
        var data = await MovieModel(m).updateOne({_id:id},{$set:{collected}});
        if(data.nModified==0){
            throw new Error(`${m}这个传值不合法`)
        }else{
            /* 添加收藏数据到user */
            var item = await MovieModel(m).find({_id:id});
            await UserModel.updateOne({},{$push:{collects:item[0]}});
            ctx.body = {
                code:200,
                msg:"收藏成功"
            }
        }
    }catch(err){
        /* id,collected  走catch */
        ctx.body = {
            code:400,
            msg:"输入的参数不合法"
        }
    }

})
module.exports = router;
```

```js
//6-2  取消收藏
TIPS:Schema一定要和数据映射
//user.js
const schema = new Schema({
     name:String,
     /* Schema中定义模糊数组,可以添加,但是不能删除 */
     collects:[
          {
               _id:Schema.Types.ObjectId,
               pic:String,
               title:String,
               slogo:String,
               evaluate:String,
               rating:String,
               collected:Boolean,
               labels:Array
          }
     ]
});
```

```js
//movieCollect.js
router.post("/api/collectMovie/:m",async ctx=>{
    var {m} = ctx.request.params;  
    var {id,collected} = ctx.request.body;
    try{
        var data = await MovieModel(m).updateOne({_id:id},{$set:{collected}});
        if(data.nModified==0){
            throw new Error(`${m}这个传值不合法`)
        }else{
            /* 添加收藏数据到user */
            var item = await MovieModel(m).find({_id:id});
            if(collected){
                /* true,$push */
                await UserModel.updateOne({},{$push:{collects:item[0]}});
                ctx.body = {
                    code:200,
                    msg:"收藏成功"
                }
            }else{
                console.log(collected)
                /* false,$pull */
                await UserModel.updateOne({},{$pull:{collects:{_id:id}}});
                ctx.body = {
                    code:200,
                    msg:"取消收藏发"
                }
            }
           
           
        }
    }catch(err){
        /* id,collected  走catch */
        ctx.body = {
            code:400,
            msg:"输入的参数不合法"
        }
    }

})
```



## 07 history

```
//GET  /api/history?id=1001&m=top250
```

```js
router.get("/api/history",async ctx=>{
    var {id,m} = ctx.query;
    try{
        var data = await MovieModel(m).find({_id:id});
        var item = data[0];
        /* 判断history中是否有这条数据 */
        var isCollect = await UserModel.find({"history._id":id});
        if(isCollect.length == 0){
                await UserModel.updateOne({$push:{history:item}});
                ctx.body = {
                    code:200,
                    msg:"添加到历史记录"
                }
        }else{
            ctx.body = {
                code:400,
                msg:"历史记录中已存在"
            }
        }
    }catch(err){
        ctx.body ={
            code:400,
            msg:"输入的参数不合法"
        }
    }
})
```

```
ui设计师 (需要童子功-从小画画)
前端(ios/android/页面仔)
后端(java/python)
测试(技术含量不高)
运维(保证整个项目在linux服务器上的正常运转,项目的发布,维护)全天候24-online
```



## 08 search

```js
//搜索
/api/search?keyword=""
var tables = ["top250","inTheaters","comingSoon"]
//模糊查询
router.get("/api/search",async ctx=>{
    var {keyword} = ctx.query;
    var reg = new RegExp(keyword);
    var tables = ["top250","inTheaters","comingSoon"];
    var searchMovies = []
    for(var item of tables){
        // var res = await MovieModel(item).find({title:{$regex:keyword}});
        var res = await MovieModel(item).find({title:reg});
        searchMovies.push(...res);
    }
    ctx.body = {
        code:200,
        res:searchMovies,
        total:searchMovies.length,
        msg:"电影搜索"
    }
})
module.exports = router;
```

## 09文章详情和电影详情

```js
//电影详情,文章详情
/api/movieDetail?id=
/api/articleDetail?id=
```

